---
layout: markdown_page
title: "Product Vision - Release"
---

- TOC
{:toc}

## Release Automation & Orchestration

The Release stage comes after your build and test pipeline is complete and
ensures that your software makes it through your review apps, pre-production
environments, and ultimately to your users with zero touch needed. It
is part of the [CI/CD](/direction/cicd) department, and includes elements of
[mobile development and delivery](/direction/mobile) use cases; if you're
interested in those topics, be sure to check out those links.

Take a look at this video for a walkthrough of the stage, north stars, and 
categories that make up Release:

<figure class="video_container">
<iframe src="https://youtube.com/embed/kdsXdNR4kQ8" frameborder="0" allowfullscreen="true" width="320" height="180"> </iframe>
</figure>

Feel free to reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) / 
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask 
any questions about what's coming.

## North Stars

There are a few important north stars we are keeping sight of to guide us forward 
in this space. With each of these, we're focusing on complete (what we call minimally 
lovable) features. Iterations are how we build software, but at the same time we 
want to continue to "water" those features that have proven their value, allowing 
them to grow into more complete and comprehensive solutions.

We track epics for many of the major deliverables associated with these north stars,
you can view them on a calendar at [this link](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Arelease&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).

##### Zero-Touch Delivery

Technologies like AutoDevOps are leading the way in making it possible for
developers to focus on building code and letting the automation handle figuring out
how to handle the deployment. Our vision is for CD to take over from your build
system and predictably manage your delivery for you while remaining completely
customizable to support any unique cases. We're achieving this through templates,
smart defaults, and feature flags that give you the confidence to release smaller
changes more frequently.

Related Epics:

- [Make CD more reliable and require less interaction](https://gitlab.com/groups/gitlab-org/-/epics/770)
- [Better management of stability in master](https://gitlab.com/groups/gitlab-org/-/epics/101)
- [CI/CD Reporting & Analytics improvements](https://gitlab.com/groups/gitlab-org/-/epics/358)
- [Make Feature Flags viable](https://gitlab.com/groups/gitlab-org/-/epics/763)
- [A/B/n testing using Feature Flags](https://gitlab.com/groups/gitlab-org/-/epics/712)

##### On-Demand Environments

Delivering software to your environments automatically is the first step, and 
new technologies like Kubernetes are now allowing on-demand environments to be
created like never before. By tying your source repo to intelligent infrastructure
that understands the intentions of developers, you can enable a true one-click 
workflow to deliver to your internal or external end users.

Related Epics:

- [Make environments easier to use and manage](https://gitlab.com/groups/gitlab-org/-/epics/767)
- [Improve certificate handling for Pages](https://gitlab.com/groups/gitlab-org/-/epics/766)

##### Secure, Compliant, and Well-Orchestrated Deployments

Releasing in GitLab takes advantage of our [deeply connected ecosystem](/handbook/product/#single-application) 
to provide traceability and auditing all the way from planning through monitoring. 
Compliance and security evidence is gathered and validated at each step of 
the way, giving you confidence that what you're delivering is what you expect.
Deployments are separated from builds and tests, ensuring a clear boundary 
between developers and production environments, thanks to your well-orchestrated
and automated delivery process.

Related Epics:

- [Lock down path to production](https://gitlab.com/groups/gitlab-org/-/epics/762)
- [Cross-project pipeline triggering and visualization](https://gitlab.com/groups/gitlab-org/-/epics/414)
- [Mature release orchestration](https://gitlab.com/groups/gitlab-org/-/epics/771)
- [Improve mobile CI/CD using GitLab](https://gitlab.com/groups/gitlab-org/-/epics/769)

<%= partial("direction/categories", :locals => { :stageKey => "release" }) %>

## Contributing

At GitLab, one of our values is that everyone can contribute. If you're looking to
get involved with features in the Release area, there are a couple searches you can use
to find issues to work on:

- [UI Polish](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Accepting%20merge%20requests&label_name[]=devops%3Arelease) items are generally frontend development (or at least minimal backend), are very self-contained, and are great, very visible items that can make a big difference when it comes to usability.
- [Accepting Merge Request](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=UI+polish&label_name%5B%5D=devops%3Arelease&scope=all&state=opened) items are open to contribution. These are generally a bit more complicated, so if you're interested in contributing we recommend you open up a dialog with us in the issue.
- [Community Contribution](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20contribution&label_name[]=devops%3Arelease) items may already have some contributors working on them if you want to join up. There aren't always issues here to find.

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

## What's Next

It's important to call out that the plan can change any moment and should not be 
taken as a hard commitment, though we do try to keep things generally stable. In 
general, we follow the same [prioritization guidelines](/handbook/product/#prioritization) 
as the product team at large. Issues will tend to flow from having no milestone, to 
being added to the backlog, to being added to this page once a specific milestone is set.

Take a look at the video below for a tour of what's coming next.

<figure class="video_container">
<iframe src="https://youtube.com/embed/G_YQYAZRccE" frameborder="0" allowfullscreen="true" width="320" height="180"> </iframe>
</figure>

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "release" }) %>
